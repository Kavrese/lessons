package com.timeapp.meditationapp.AuthScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.timeapp.meditationapp.*
import com.timeapp.meditationapp.MainScreen.MainActivity
import com.timeapp.meditationapp.RegScreen.RegActivity
import com.timeapp.meditationapp.common.*
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.activity_auth.to_reg
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        to_reg.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }

        auth.setOnClickListener {
            val email = email_auth.text.toString()
            val password = password_auth.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()) {
                if (validateEmail(email)) {
                    val api = getRetrofit().create(SomeApi::class.java)

                    api.login(UserData(email, password)).enqueue(object : Callback<ResponseLogin> {
                        override fun onResponse(call: Call<ResponseLogin>?, response: retrofit2.Response<ResponseLogin>?) {
                            if (response!!.isSuccessful) {
                                val sh = getPref(this@AuthActivity)
                                val ed = sh.edit()
                                ed.putString("token", response.body().token)
                                ed.putString("avatar", response.body().avatar)
                                ed.putString("email", response.body().email)
                                ed.putString("username", response.body().nickname)
                                ed.commit()
                                startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                            } else {
                                Toast.makeText(this@AuthActivity, response!!.message(), Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseLogin>?, t: Throwable?) {
                            Toast.makeText(this@AuthActivity, t!!.message, Toast.LENGTH_SHORT).show()
                        }
                    })
                }else{
                    Toast.makeText(this, "Почта не корректна", Toast.LENGTH_LONG).show()
                }
            }else{
                Toast.makeText(this, "Заполните все поля", Toast.LENGTH_LONG).show()
            }
        }
    }
}