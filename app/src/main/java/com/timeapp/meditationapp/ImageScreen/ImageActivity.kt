package com.timeapp.meditationapp.ImageScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.bumptech.glide.Glide
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.common.list_path_img
import com.timeapp.meditationapp.common.myAdapter
import com.timeapp.meditationapp.common.uri
import kotlinx.android.synthetic.main.activity_image.*
import java.util.*

class ImageActivity : AppCompatActivity() {
    var b = false
    var xp = -999f
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)//
        setContentView(R.layout.activity_image)
        var time1 = Calendar.getInstance().time.time-1000

        photo_view.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                if (p1!!.action == MotionEvent.ACTION_DOWN) {
                    xp = p1.x
                } else if (p1!!.action == MotionEvent.ACTION_UP) {
                    if (xp-p1.x >= 500f) { deleting() }
                    else {
                        if (time1 + 200 >= Calendar.getInstance().time.time) {
                            if (b) {
                                photo_view.animate()
                                    .setDuration(200)
                                    .scaleX(1f)
                                    .scaleY(1f)
                                    .start()
                                b = false
                            } else {
                                photo_view.animate()
                                    .setDuration(200)
                                    .scaleX(2f)
                                    .scaleY(2f)
                                    .start()
                                b = true
                            }
                        } else {
                            time1 = Calendar.getInstance().time.time
                        }
                    }
                }
                return true
            }
        })

        Glide.with(photo_view)
            .load(uri)
            .into(photo_view)
    }

    fun onDeleteClicked(v : View) {
        deleting()
    }

    fun deleting() {
        list_path_img.remove(uri)
        myAdapter.notifyDataSetChanged()
        finish()
    }
}