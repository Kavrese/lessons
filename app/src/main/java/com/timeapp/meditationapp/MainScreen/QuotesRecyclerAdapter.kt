package com.timeapp.meditationapp.MainScreen

import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.common.DataQuotes
import kotlinx.android.synthetic.main.item_two.view.*

class QuotesRecyclerAdapter(list: List<DataQuotes>) : RecyclerView.Adapter<QuotesRecyclerAdapter.VH>() {
    val list: List<DataQuotes> = list

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.textView7
        val descr = itemView.textView8
        val image = itemView.imageView8
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_two, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.name.text = list[position].title
        holder.descr.text = list[position].description
        val b = Base64.decode(list[position].image, Base64.DEFAULT)
        holder.image.setImageBitmap(BitmapFactory.decodeByteArray(b, 0, b.size))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}