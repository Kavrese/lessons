package com.timeapp.meditationapp.OnBoardingScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.timeapp.meditationapp.AuthScreen.AuthActivity
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.RegScreen.RegActivity
import com.timeapp.meditationapp.common.getPref
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        val sh = getPref(this)
        val name = sh.getString("name", "")
        if (name != "")
            hello_text.text = hello_text.text.toString() + ", " + name

        to_auth.setOnClickListener {
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }

        to_reg.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }
    }
}