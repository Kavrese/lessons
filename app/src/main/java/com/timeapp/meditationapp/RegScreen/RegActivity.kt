package com.timeapp.meditationapp.RegScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.common.getPref
import kotlinx.android.synthetic.main.activity_reg.*

class RegActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)

        val sh = getPref(this)

        save_name.setOnClickListener {
            sh.edit()
                .putString("name", text_name.text.toString())
                .apply()
            Toast.makeText(this, "Имя сохраненно", Toast.LENGTH_LONG).show()
        }
    }
}