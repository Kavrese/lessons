package com.timeapp.meditationapp.SplashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.timeapp.meditationapp.OnBoardingScreen.OnBoardingActivity
import com.timeapp.meditationapp.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
                startActivity(Intent(this@SplashActivity,OnBoardingActivity::class.java))
                finish()
            }, 1500)
    }
}