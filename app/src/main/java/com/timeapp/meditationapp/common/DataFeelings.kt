package com.timeapp.meditationapp.common

data class DataFeelings(
    val id: String,
    val title: String,
    val position: String,
    val image: String
)
