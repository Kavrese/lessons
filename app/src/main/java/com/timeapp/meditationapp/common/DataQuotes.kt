package com.timeapp.meditationapp.common

data class DataQuotes(
    val id: String,
    val title: String,
    val description: String,
    val image: String
)
