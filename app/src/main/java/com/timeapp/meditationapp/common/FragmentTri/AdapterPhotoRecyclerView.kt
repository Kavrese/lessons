package com.timeapp.meditationapp.common.FragmentTri

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.timeapp.meditationapp.R
import android.widget.ImageView
import com.timeapp.meditationapp.ImageScreen.ImageActivity
import com.timeapp.meditationapp.common.list_path_img
import com.timeapp.meditationapp.common.uri

class AdapterPhotoRecyclerView(val onClickListener: View.OnClickListener, val activity: Activity): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.findViewById<ImageView>(R.id.img)
    }

    class AddPhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val add = itemView.findViewById<TextView>(R.id.add)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0)
            PhotoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rec_photo, parent, false))
        else
            AddPhotoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rec_add_photo, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == 0){
            val bitmapImage =
                MediaStore.Images.Media.getBitmap(activity.contentResolver, list_path_img[position])
            (holder as PhotoViewHolder).image.setImageBitmap(bitmapImage)
            holder.itemView.setOnClickListener {
                uri = list_path_img[position]
                holder.itemView.context.startActivity(Intent(holder.itemView.context, ImageActivity::class.java))
            }
        }else{
            (holder as AddPhotoViewHolder).add.setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount(): Int = list_path_img.size + 1

    override fun getItemViewType(position: Int): Int {
        return if (position != list_path_img.size) 0 else 1
    }

    fun addImage(image: Uri){
        list_path_img.add(image)
        notifyDataSetChanged()
    }
}