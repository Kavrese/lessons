package com.timeapp.meditationapp.common

data class ResponseFeelings(
    val success: Boolean,
    val data: List<DataFeelings>
)
