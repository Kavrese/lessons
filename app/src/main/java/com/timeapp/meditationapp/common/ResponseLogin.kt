package com.timeapp.meditationapp.common

data class ResponseLogin(
    val email : String,
    val avatar : String,
    val id : String,
    val nickname : String,
    val token : String
)
