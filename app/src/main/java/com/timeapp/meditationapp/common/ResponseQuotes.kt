package com.timeapp.meditationapp.common

data class ResponseQuotes(
    val success: Boolean,
    val data: List<DataQuotes>
)
