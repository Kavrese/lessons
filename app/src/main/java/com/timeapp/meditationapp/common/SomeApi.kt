package com.timeapp.meditationapp.common

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface SomeApi {
    @POST("user/login")
    fun login(@Body user: UserData): Call<ResponseLogin>

    @GET("feelings")
    fun feel(): Call<ResponseFeelings>

    @GET("quotes")
    fun quo(): Call<ResponseQuotes>
}