package com.timeapp.meditationapp.common

data class UserData(
    val email : String,
    val password : String
)
