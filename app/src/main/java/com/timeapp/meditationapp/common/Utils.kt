package com.timeapp.meditationapp.common

import android.app.Activity
import android.content.SharedPreferences
import android.net.Uri
import com.timeapp.meditationapp.common.FragmentTri.AdapterPhotoRecyclerView
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

lateinit var uri: Uri
lateinit var myAdapter: AdapterPhotoRecyclerView
val list_path_img = mutableListOf<Uri>()

fun validateEmail(email: String): Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun getRetrofit() : Retrofit {
    return Retrofit.Builder()
        .baseUrl("http://46.138.242.199:1111/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun getPref(a: Activity) : SharedPreferences {
    return a.getSharedPreferences("0", 0)
}