package com.timeapp.meditationapp.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.timeapp.meditationapp.MainScreen.FeelingsRecyclerAdapter
import com.timeapp.meditationapp.MainScreen.QuotesRecyclerAdapter
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.common.*
import kotlinx.android.synthetic.main.fragment_first.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FragmentFirst: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sh = getPref(requireActivity())

        hello_user_text.text = "С возвращением, ${sh.getString("username", "")}"
        val b = Base64.decode(sh.getString("avatar", ""), Base64.DEFAULT)
        imageView11.setImageBitmap(BitmapFactory.decodeByteArray(b, 0, b.size))

        val retrofit = getRetrofit()

        val api = retrofit.create(SomeApi::class.java)

        api.feel().enqueue(object: Callback<ResponseFeelings> {
            override fun onResponse(call: Call<ResponseFeelings>?, response: Response<ResponseFeelings>?) {
                if (response!!.isSuccessful) {
                    val adapter = FeelingsRecyclerAdapter(response.body().data)
                    feelingsRecycler.adapter = adapter
                } else {
                    Toast.makeText(requireContext(), response.message(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ResponseFeelings>?, t: Throwable?) {
                Toast.makeText(requireContext(), t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
        api.quo().enqueue(object : Callback<ResponseQuotes> {
            override fun onResponse(call: Call<ResponseQuotes>?, response: Response<ResponseQuotes>?) {
                if (response!!.isSuccessful) {
                    val adapter = QuotesRecyclerAdapter(response.body().data)
                    quotesRecycler.adapter = adapter
                } else {
                    Toast.makeText(requireContext(), response!!.message(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ResponseQuotes>?, t: Throwable?) {
                Toast.makeText(requireContext(), t!!.message, Toast.LENGTH_SHORT).show()
            }

        })
    }
}