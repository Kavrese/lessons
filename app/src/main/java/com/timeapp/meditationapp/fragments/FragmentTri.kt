package com.timeapp.meditationapp.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.timeapp.meditationapp.R
import com.timeapp.meditationapp.common.FragmentTri.AdapterPhotoRecyclerView
import com.timeapp.meditationapp.common.myAdapter
import kotlinx.android.synthetic.main.fragment_treed.*


class FragmentTri: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_treed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        myAdapter = AdapterPhotoRecyclerView(
            {
                val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(cameraIntent, 1000)
            }, requireActivity())
        rec_photos.apply {
            adapter = myAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {
                val returnUri = data!!.data!!
                (rec_photos.adapter as AdapterPhotoRecyclerView).addImage(returnUri)
            }
        }
    }
}